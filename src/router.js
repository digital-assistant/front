import Vue from 'vue';
import Router from 'vue-router';
import store from './store.js';

Vue.use(Router);

function requireAuth (to, from, next) {
    if (store.getters.auth.token) {
        next();
    } else {
        next(false);
    }
}

function requireGuest (to, from, next) {
    if (store.getters.auth.token) {
        next(false);
    } else {
        next();
    }
}

export default new Router({
    mode: 'history',
    base: process.env.BASE_URL,
    routes: [
        {
            path: '/',
            name: 'home',
            component: () => import('./views/Home.vue')
        },
        {
            path: '/register',
            name: 'register',
            component: () => import('./views/Auth/Register.vue'),
            beforeEnter: requireGuest
        },
        {
            path: '/login',
            name: 'login',
            component: () => import('./views/Auth/Login.vue'),
            beforeEnter: requireGuest
        },
        {
            path: '/about',
            name: 'about',
            component: () => import('./views/About.vue')
        },
        {
            path: '/dashboard',
            name: 'dashboard',
            component: () => import('./views/Dashboard.vue'),
            beforeEnter: requireAuth
        },
        {
            path: '/logout',
            redirect: { name: 'home' }
        },
        {
            path: '/profile',
            name: 'profile',
            component: () => import('./views/Profile/Profile.vue'),
            beforeEnter: requireAuth
        },
        {
            path: '/my-meetings',
            name: 'my-meetings',
            component: () => import('./views/Meetings/Meetings.vue'),
            beforeEnter: requireAuth
        },
        {
            path: '/profile/update',
            name: 'update-profile',
            component: () => import('./views/Profile/UpdateProfile.vue'),
            beforeEnter: requireAuth
        },
        {
            path: '/schedule',
            name: 'schedule',
            component: () => import('./views/Schedule/Schedule.vue'),
            beforeEnter: requireAuth
        },
        {
            path: '/schedule/update',
            name: 'update-schedule',
            component: () => import('./views/Schedule/UpdateSchedule.vue'),
            beforeEnter: requireAuth
        },
        {
            path: '/schedule-meeting/:id',
            name: 'schedule-meeting',
            component: () => import('./views/Schedule/ScheduleMeeting.vue'),
            beforeEnter: requireAuth
        },
        {
            path: '/select-meeting/:id',
            name: 'select-meeting',
            component: () => import('./views/Schedule/SelectMeeting.vue'),
            beforeEnter: requireAuth
        },
        {
            path: '*',
            name: 'not-found',
            component: () => import('./views/errors/404.vue')
        }
    ]
});
