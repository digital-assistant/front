import Vue from 'vue';
import Vuex from 'vuex';
import createPersist from 'vuex-localstorage';

Vue.use(Vuex);

export default new Vuex.Store({
    plugins: [createPersist({
        namespace: 'sessions-medical',
        initialState: {},
        // ONE_WEEK
        expires: 2 * 60 * 60 * 1e3
    })],
    state: {
        auth: {},
        user: {}
    },
    mutations: {
        login (state, auth) {
            state.auth = auth;
        },
        setUser (state, user) {
            state.user = user;
        },
        logout (state) {
            state.auth = {};
            state.user = {};
        }
    },
    getters: {
        auth: state => {
            return state.auth;
        },
        user: state => {
            return state.user;
        }
    }
});
