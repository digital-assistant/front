import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import VueSocketIO from 'vue-socket.io';
import underscore from 'underscore';
import moment from 'moment';

function getTimeStops (start, end, minutes) {
    let startTime = moment(start, 'HH:mm');
    let endTime = moment(end, 'HH:mm');

    if (endTime.isBefore(startTime)) {
        endTime.add(1, 'day');
    }

    let timeStops = [];
    while (startTime <= endTime) { // eslint-disable-line
        timeStops.push(new moment(startTime).format('HH:mm')); // eslint-disable-line new-cap
        startTime.add(minutes, 'minutes');
    }
    return timeStops;
}

window._ = underscore;
window.moment = moment;
window.getTimeStops = getTimeStops;

Vue.config.productionTip = false;
Vue.use(new VueSocketIO({
    // debug: true,
    connection: `${process.env.VUE_APP_SOCKET_URI}/`,
    vuex: {
        store,
        actionPrefix: 'SOCKET_',
        mutationPrefix: 'SOCKET_'
    }
}));

new Vue({
    router,
    store,
    render: h => h(App)
}).$mount('#app');
