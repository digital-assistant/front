module.exports = [
    {
        socket: {
            call: 'schedule.create', done: 'schedule.created', error: 'schedule.create.error'
        },
        rabbitmq: {
            exchange: 'schedules', call: 'schedule.create', done: 'schedule.created', error: 'schedule.create.error'
        }
    },
    {
        socket: {
            call: 'schedule.get.by.params', done: 'schedule.got.by.params', error: 'schedule.get.by.params.error'
        },
        rabbitmq: {
            exchange: 'schedules', call: 'schedule.get.by.params',
            done: 'schedule.got.by.params', error: 'schedule.get.by.params.error'
        }
    }
];

