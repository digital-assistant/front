module.exports = [
    {
        socket: {
            call: 'user.create', done: 'user.created', error: 'user.create.error'
        },
        rabbitmq: {
            exchange: 'users', call: 'user.create', done: 'user.created', error: 'user.create.error'
        }
    },
    {
        socket: {
            call: 'user.authenticate', done: 'user.authenticated', error: 'user.authenticate.error'
        },
        rabbitmq: {
            exchange: 'users', call: 'user.authenticate', done: 'user.authenticated', error: 'user.authenticate.error'
        }
    },
    {
        socket: {
            call: 'user.get.by.auth-token', done: 'user.got.by.auth-token', error: 'user.get.by.auth-token.error'
        },
        rabbitmq: {
            exchange: 'users', call: 'user.get.by.auth-token', done: 'user.got.by.auth-token', error: 'user.get.by.auth-token.error'
        }
    },
    {
        socket: {
            call: 'user.update', done: 'user.updated', error: 'user.update.error'
        },
        rabbitmq: {
            exchange: 'users', call: 'user.update', done: 'user.updated', error: 'user.update.error'
        }
    },
    {
        socket: {
            call: 'user.delete', done: 'user.deleted', error: 'user.delete.error'
        },
        rabbitmq: {
            exchange: 'users', call: 'user.delete', done: 'user.deleted', error: 'user.delete.error'
        }
    },
    {
        socket: {
            call: 'user.get.all', done: 'user.got.all', error: 'user.get.all.error'
        },
        rabbitmq: {
            exchange: 'users', call: 'user.get.all', done: 'user.got.all', error: 'user.get.all.error'
        }
    },
    {
        socket: {
            call: 'user.get.by.id', done: 'user.got.by.id', error: 'user.get.by.id.error'
        },
        rabbitmq: {
            exchange: 'users', call: 'user.get.by.id', done: 'user.got.by.id', error: 'user.get.by.id.error'
        }
    },
    {
        socket: {
            call: 'user.get.by.params', done: 'user.got.by.params', error: 'user.get.by.params.error'
        },
        rabbitmq: {
            exchange: 'users', call: 'user.get.by.params', done: 'user.got.by.params', error: 'user.get.by.params.error'
        }
    },
    {
        socket: {
            call: 'user.get.avail.day', done: 'user.got.avail.day', error: 'user.get.avail.day.error'
        },
        rabbitmq: {
            exchange: 'users', call: 'user.get.avail.day', done: 'user.got.avail.day', error: 'user.get.avail.day.error'
        }
    },
];

