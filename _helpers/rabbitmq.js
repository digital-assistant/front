const publish = require('amqplib').connect(process.env.RABBITMQURI + '?heartbeat=60');
const consume = require('amqplib').connect(process.env.RABBITMQURI + '?heartbeat=60');


function getFromMQ(exchange, key, doAction, type='topic') {
    consume.then(conn => {
        return conn.createChannel();
    }).then(ch => {
        ch.assertExchange(exchange, type, {durable: false}).then(async () => {
            const q = await ch.assertQueue('', { exclusive: false });
            ch.bindQueue(q.queue, exchange, key).then(() => {
                ch.consume(q.queue, doAction, { noAck: true });
            });
        });
    }).catch(console.warn);
}

function sendToMQ(key, data, type='topic', exchange='front') {
    publish.then(conn => {
        return conn.createChannel();
    }).then(async ch => {
        const ok = await ch.assertExchange(exchange, type, { durable: false });
        return ch.publish(exchange, key, Buffer.from(JSON.stringify(data)));
    }).catch(console.warn);
}

module.exports = {
    getFromMQ,
    sendToMQ
};
