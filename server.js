require('rootpath')();
const express = require('express');
const app = express();
const server = require('http').Server(app);
const frontService = require('lib/front.rabbitmq');
const path = require('path');
const io = require('socket.io')(server);

app.use('/', express.static(path.join(__dirname, 'dist')));

app.get('*', function (req, res) {
    res.sendFile(path.join(__dirname, '/dist/index.html'));
});

io.on('connection', socket => {
    console.log('connectd', socket);
    if (process.env.RABBITMQURI) {
        frontService(socket);
    } else {
        console.error('undifiend rabbitmq URI!');
    }
});

const port = process.env.PORT || 80;

server.listen(port);
